#!/usr/bin/env bash
###
#File: start_remote_jupyter_server.sh
#Created: Thursday, 6th October 2022 6:21:43 pm
#Author: Jannek Squar (jannek.squar@uni-hamburg.de)
#-----
#
#-----
#Last Modified: Friday, 7th October 2022 12:30:38 pm
#Modified By: Jannek Squar (jannek.squar@uni-hamburg.de)
#-----
#Copyright (c) 2022 Jannek Squar
#
###

: ${USER_ACCOUNT:=$2}
: ${GROUP_ACCOUNT:=$3}
: ${ENV_NAME:=conda_remote}
: ${SERVER:=$1}
: ${SLURM_PARTITION:=$4}

function usage {
	echo "Call script like this:"
	echo "./start_remote_jupyter_server.sh SERVER USER_ACCOUNT GROUP_ACCOUNT SLURM_PARTITION"
	echo ""
	echo "SERVER: Server address"
	echo "USER_ACCOUNT: Your user account on the server"
	echo "GROUP_ACCOUNT: Your group account, via which you may submit SLURM jobs on the server"
	echo "SLURM_PARTITION: SLURM partition, which shall be used for job allocation"
	exit 1
}

# Step 0: Check usage of script
if [[ "$#" -ne 4 ]]; then
	usage
fi

# Step 1: Setup server environment:
	# 1. Check conda environment
	# 2. Create SLURM job script
	# 3. Submit SLURM job with Jupyter Lab server
	# 4. Wait until job is running

ssh -T $USER_ACCOUNT@$SERVER <<-EOL
	# adjust the following line to your needs. On my system conda is loaded by the python3 module
	module load python3

	echo "+++++++++++++++++++++++++++++++++++++++++++"
	echo "Check conda environment"
	echo "+++++++++++++++++++++++++++++++++++++++++++"
	# setup conda environment
	if [[ ! \$(conda info --env | grep $ENV_NAME) ]]; then
		echo "Conda environment $ENV_NAME has not been found and will now be created"
		conda create --yes --name $ENV_NAME
		echo "Install jupyterlab in environment $ENV_NAME"
		conda install --yes --name $ENV_NAME -c conda-forge jupyterlab
		conda init bash
	else
		echo "Conda environment $ENV_NAME has been found"
	fi
	echo "Activate conda environment $ENV_NAME"
	source \$HOME/.bashrc
	conda activate $ENV_NAME

	# create slurm script
	# make env variables available within nested here-document, 
	# since it will created on the server and local variables are therefore lost
	GROUP_ACCOUNT=$GROUP_ACCOUNT
	SLURM_PARTITION=$SLURM_PARTITION
	echo "+++++++++++++++++++++++++++++++++++++++++++"
	echo "Submit SLURM job for Group Account \$GROUP_ACCOUNT on partition \$SLURM_PARTITION"
	echo "+++++++++++++++++++++++++++++++++++++++++++"

	# check if job script already exists
	if [[ ! -f start_jupyter_server.sh ]]; then
		echo "Create job script start_jupyter_server.sh"
		cat <<-EOL_NESTED > start_jupyter_server.sh
			#!/bin/bash
			# launch jupyter

			#SBATCH --account=\$GROUP_ACCOUNT
			#SBATCH -J jupyter-server
			#SBATCH --time=8:00:00
			#SBATCH --mem=5G
			#SBATCH --partition=\$SLURM_PARTITION

			# Setup Environment
			source ~/.bashrc
			conda activate $ENV_NAME

			jupyter lab --no-browser --ip "*" --notebook-dir \$HOME
		EOL_NESTED
	else
		echo "Reuse existing job script start_jupyter_server.sh"
	fi

	# submit SLURM job
	JOB_ID=\$(sbatch start_jupyter_server.sh | cut -d " " -f 4)

	echo "+++++++++++++++++++++++++++++++++++++++++++"
	echo "Wait until job \$JOB_ID is running"
	echo "+++++++++++++++++++++++++++++++++++++++++++"

	while true; do
		JOB_QUEUE=\$(squeue |grep \$JOB_ID)
		NODE=\$(echo \$JOB_QUEUE | cut -d " " -f 8)
		STATE=\$(echo \$JOB_QUEUE | cut -d " " -f 5)
		sleep 1s
		if [[ -z \$STATE ]]; then
			echo "Queue: \$JOB_QUEUE"
			echo "State for job \$JOB_ID is unknown. Terminate script"
			exit 1
		fi
		if [[ \$STATE == R ]]; then
			echo ""
			echo "Job \$JOB_ID is running on node \$NODE"
			break
		else
			echo -n "."
		fi
	done
	echo ""
	echo "You need now to create a tunnel to the allocated node on $SERVER if not already happened."
	echo "You can connect via your local webbrowser via the URL localhost:8888."
	echo "Do not forget to cancel your job on $SERVER after you are finished."
	echo "If the default port 8888 is not used by jupyter, you need to lookup the correct port in the SLURM output file and adjust remote_port in localport:localhost:remote_port of the ssh command accordingly."	
	echo ""
	echo "+++++++++++++++++++++++++++++++++++++++++++"
	echo "Use the following command to create the tunnel:"
	echo "ssh -L 8888:localhost:8888 -J $USER_ACCOUNT@$SERVER $USER_ACCOUNT@\$NODE"
	echo "+++++++++++++++++++++++++++++++++++++++++++"
EOL


