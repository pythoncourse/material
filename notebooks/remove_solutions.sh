#!/usr/bin/env bash
set -e
#------------------------------------------------------------------------------
#
# Remove the exercise solutions in the input notebook and write the output to 
# a new file without the '_with_solutions' part in the file name. 
#
# Input file name has to be
#
#       name_with_solution.ipynb
#
# Output file name will be
#
#       name.ipynb
#
# The individual solution codes are enclosed between the following lines in the 
# notebook:
# 
#   '# EXERCISE START'
#
#   and
#
#   '# EXERCISE END' 
#
# Example:
# --------
#
# infile:               visualization_intro_part1_with_solutions.ipynb
#
# generated outfile:    visualization_intro_part1.ipynb
#
#------------------------------------------------------------------------------

#-- Input file name
infile=$1

#-- first, clear the output cells of the input notebook because gitlab does not 
#-- display large notebooks anymore
jupyter nbconvert --clear-output --inplace $infile

#-- Generate the output file name
outname=$(echo ${infile%.ipynb} | sed -e 's/_with_solutions//g')

outfile=${outname}.ipynb
echo "Output file: $outfile"

#-- Delete the exercise solutions. An empty code cell remains in the notebook.

#-- Display the sed command line
echo "cat ${infile} | sed '/# EXERCISE START/,/# EXERCISE END/d' > ${outfile}"

cat ${infile} | sed '/# EXERCISE START/,/# EXERCISE END/d' > ${outfile}

# exit
