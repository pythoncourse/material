# 'notebooks' folder

The notebooks folder contains the Jupyter notebooks used in the DKRZ Python course.

Subfolder:

    applications:   Notebooks showing used cases
    
    supplements:    additional notebooks (lectures)
    