# Supplements

The supplements folder contains additional materials.

## Content

### Introductions

<table><col style="width: 300px;">
   <tr><th style="text-align: left;"> Subject </th>
       <th  style="text-align: left;"> notebook/script </th>
   </tr>
   <tr>
      <td> CDO introduction </td>
      <td> cdo_introduction.ipynb </td>
   </tr>
   <tr>
      <td> Conda introduction </td>
      <td> conda_introduction.ipynb </td>
   </tr>
   <tr>
      <td> Pandas introduction </td>
      <td> pandas_introduction.ipynb </td>
   </tr>
   <tr>
      <td> Git introduction </td>
      <td> git_intro.ipynb </td>
   </tr>
</table>   

<br>

### Others

<table><col style="width: 300px;">
   <tr><th style="text-align: left;"> Subject </th>
       <th  style="text-align: left;"> notebook/script </th>
   </tr>
   <tr>
      <td> About date and time </td>
      <td> date_and_Time.ipynb </td>
   </tr>
   <tr>
      <td> Multiple plots in one figure </td>
      <td> combine_multiple_plots_in_one_figure.ipynb </td>
   </tr>
   <tr>
      <td> Create an animation and save it to MPEG-4 </td>
      <td> animation_save_to_mpeg4.ipynb </td>
   </tr>
   <tr>
      <td> Xarray irregular grids </td>
      <td> Xarray_IrregularGrids.ipynb </td>
   </tr>
   <tr>
      <td> Derotate rotated pole data </td>
      <td> derotate_curvilinear_grid_with_rotated_pole.ipynb </td>
   </tr>
   <tr>
      <td> Use TIFF image for map background </td>
      <td> map_background_images.ipynb </td>
   </tr>
   <tr>
      <td> Work with pre-projected data </td>
      <td> pre-projected_data.ipynb </td>
   </tr>
   <tr>
      <td> Use a shapefile to mask data </td>
      <td> mask_data_using_shapefile_polygons.ipynb </td>
   </tr>
</table>



