{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f9786a29-07ff-45c7-9020-50c3a685d132",
   "metadata": {},
   "source": [
    "# DKRZ Python visualization\n",
    "\n",
    "## How to plot pre-projected stereographic data\n",
    "\n",
    "----\n",
    "\n",
    "```\n",
    "Copyright 2022 Deutsches Klimarechenzentrum GmbH (DKRZ)\n",
    "Licensed under CC-BY-NC-SA-4.0\n",
    "```\n",
    "\n",
    "----\n",
    "\n",
    "<img src=\"../../images/plot_pre-projected_data_scale_0.98.png\" alt=\"Pre-projection scale_factor=0.98\" align=\"right\" width=\"30%\">\n",
    "\n",
    "\n",
    "There is not always the need to regrid the data to a rectilinear lonlat grid before plotting. Cartopy provides multiple projections which can be used to plot the data on a map using its native grid.\n",
    "\n",
    "In this example we demonstrate how to read and plot the pre-projected polar-stereographic permafrost data from <a href=\"https://climate.esa.int/en/odp/#/project/permafrost\"><b>ESA CCI</b></a> (ESA Climate Change Initiative). The coordinates **x** and **y** are in **meters** instead of lon-lat degrees and therefore we can use the <a href=\"https://proj.org/index.html\"><b>PROJ</b></a> (a generic coordinate transformation software) parameters to specify the map projection as precisely as possible.\n",
    "\n",
    "    Number of cells:  ~152.8 million\n",
    "    Variable:         ALT(time:1, x:14762, y:10353)\n",
    "\n",
    "**Content**\n",
    "- Open the dataset\n",
    "- Read the projection information\n",
    "- Create the globe\n",
    "  - 1. Plot with `scale_factor = 1`\n",
    "  - 2. Plot with `scale_factor = 0.98`\n",
    "  - 3. Make it faster\n",
    "\n",
    "**Used packages**\n",
    "- xarray\n",
    "- python-cdo\n",
    "- matplotlib\n",
    "- cartopy\n",
    "- pandas\n",
    "- numpy\n",
    "- datashader\n",
    "\n",
    "**Data download**<br>\n",
    "<a href=\"https://data.ceda.ac.uk/neodc/esacci/permafrost/data/active_layer_thickness/L4/area4/pp/v03.0/\"> data.ceda.ac.uk </a>\n",
    "\n",
    "<br>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15551659-3be5-4a9b-83a3-d6d21c93be2c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os, time\n",
    "import xarray as xr\n",
    "import numpy as np\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import cartopy.crs as ccrs\n",
    "import cartopy.feature as cfeature\n",
    "\n",
    "from cdo import Cdo\n",
    "cdo = Cdo()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5254ae2d-5652-4acc-8c79-4b5d489c9a28",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Open the dataset\n",
    "\n",
    "The data is stored in a netCDF file that can be easily loaded with **Xarray's** `open_dataset()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c536c7d8-7c79-4e92-974e-4e63ff7f483b",
   "metadata": {},
   "outputs": [],
   "source": [
    "infile = os.environ['HOME']+\\\n",
    "         '/CDO/Support/projections/ESACCI-PERMAFROST-L4-ALT-MODISLST_CRYOGRID-AREA4_PP-2003-fv03.0.nc'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8aeecb5-5054-49fa-8d1a-d28b66b0a681",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.open_dataset(infile)\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d15dc0e-6693-49ee-bfc8-ea14614efaef",
   "metadata": {},
   "source": [
    "Assign a varaible _var_ to the data variable _ALT_."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c725f38c-2cf5-42f6-8100-ce2bcd29cf01",
   "metadata": {},
   "outputs": [],
   "source": [
    "var = ds.ALT\n",
    "var"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "526571c4-fd40-481d-9a59-a310e596ded9",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Number of cells: ', (len(ds.x) * len(ds.y)))\n",
    "print('x: ', ds.x.min().values, ds.x.units, '-', ds.x.max().values, ds.x.units)\n",
    "print('y: ', ds.y.min().values, ds.y.units, '-', ds.y.max().values, ds.y.units)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bccca584-926a-4aa2-b8fb-880bcd23001b",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Read the projection information\n",
    "\n",
    "Get more information about the grid from the file using **CDO's operator** `griddes`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2de6619e-e9a4-415e-ae76-895e06a0cb1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "cdo.griddes(input=infile)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ab4de02-e6ca-4964-8d17-a777de1ed491",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "The Proj parameters for the stereographic grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7138920e-10a3-4926-a297-b8c6faa136d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "proj_params = \"+proj=stere +lat_0=90 +lat_ts=71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8fc794f9-ab93-47ea-a5b5-e63666e2763b",
   "metadata": {},
   "source": [
    "To make it a bit more handy we create a dictionary from the PROJ parameter list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "422477a8-7c10-4e6d-a71d-3dab839eea98",
   "metadata": {},
   "outputs": [],
   "source": [
    "p = proj_params.split(' ')\n",
    "\n",
    "pdict = {}\n",
    "for i,s in enumerate(p):\n",
    "    if i < len(p)-1:\n",
    "        pdict[s[1:].split('=')[0]] = s.split('=')[1]\n",
    "#pdict"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54da1e16-856c-462c-97f4-244bf56e2347",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Create the globe\n",
    "\n",
    "Create the globe used to define the stereographic projection.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a23da6e0-f651-45e9-b3b5-cfbcffca8dc0",
   "metadata": {},
   "outputs": [],
   "source": [
    "globe = ccrs.Globe(datum=pdict['datum'], \n",
    "                   #ellipse='WGS84', \n",
    "                   semimajor_axis=6378137.,\n",
    "                   inverse_flattening=298.257223563)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26d751f2-30ae-4586-baac-746bf160ec87",
   "metadata": {},
   "source": [
    "### Conversion factor pixel in inches"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fe12867c-6863-4a8a-95e2-344802260608",
   "metadata": {},
   "outputs": [],
   "source": [
    "px = 1/plt.rcParams['figure.dpi']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13b9d886-47f6-45f8-8950-2cc7e6c766b9",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "### 1. Plot with `scale_factor = 1` \n",
    "\n",
    "<img src=\"../../images/plot_pre-projected_data_scale_1.png\" alt=\"Pre-projection scale_factor=1.0\" align=\"right\" width=\"30%\">\n",
    "\n",
    "This computation takes some time (~169 s) using pcolormesh.\n",
    "\n",
    "The first plot is created by the given values for the projection. But here you can see that the data is not drawn exactly fitting the coastlines. This is often due to a rounding error of the scale_factor. The scale_factor given by the file is 1, but we have to decrease it a bit to put the data to the correct locations.\n",
    "\n",
    "The following cell is of type `Raw` which means it won't be executed when you run the cell. To run the cell change the cell type to `Code`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85c6575a-d22a-4b97-883a-e0defbbc1e9c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note: this takes some time!\n",
    "\n",
    "t1 = time.time()\n",
    "\n",
    "projection = ccrs.Stereographic(central_longitude=float(pdict['lon_0']), \n",
    "                                central_latitude=float(pdict['lat_0']),\n",
    "                                false_easting=float(pdict['x_0']),\n",
    "                                false_northing=float(pdict['y_0']), \n",
    "                                scale_factor=float(pdict['k']),\n",
    "                                globe=globe)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(800*px, 800*px), subplot_kw=dict(projection=projection))\n",
    "\n",
    "ax.set_extent([0,360,60,90], ccrs.PlateCarree())\n",
    "ax.add_feature(cfeature.LAND, facecolor='dimgray')\n",
    "#ax.add_feature(cfeature.OCEAN, facecolor='gainsboro')\n",
    "ax.gridlines(draw_labels=True)\n",
    "ax.coastlines(resolution='10m')\n",
    "ax.set_title(var.standard_name, fontsize=20, loc='left')\n",
    "\n",
    "plot = ax.pcolormesh(ds.x, ds.y, var[0,:,:], \n",
    "                     vmin=0,\n",
    "                     vmax=4,\n",
    "                     cmap='Blues')\n",
    "\n",
    "cbar = plt.colorbar(plot, ax=ax, shrink=0.4)\n",
    "\n",
    "fig.savefig('./plots/plot_pre-projected_data_scale_1.png', bbox_inches='tight')\n",
    "\n",
    "t2 = time.time()\n",
    "res = t2-t1\n",
    "print(res, 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ee7dc22-af23-4380-9f12-4612bb82f5f2",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "### 2. Plot with `scale_factor = 0.98` \n",
    "\n",
    "<img src=\"../../images/plot_pre-projected_data_scale_0.98.png\" alt=\"Pre-projection scale_factor=0.98\" align=\"right\" width=\"30%\">\n",
    "\n",
    "This computation takes some time (~182 s) using pcolormesh.\n",
    "\n",
    "The following cell is of type `Raw` which means it won't be executed when you run the cell. To run the cell switch the cell type to `Code`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "61b20f57-4da8-4f3f-80a9-210547fecd87",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note: this takes some time!\n",
    "\n",
    "t1 = time.time()\n",
    "\n",
    "projection = ccrs.Stereographic(central_longitude=float(pdict['lon_0']), \n",
    "                                central_latitude=float(pdict['lat_0']),\n",
    "                                false_easting=float(pdict['x_0']),\n",
    "                                false_northing=float(pdict['y_0']), \n",
    "                                scale_factor=0.98,\n",
    "                                globe=globe)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(800*px, 800*px), subplot_kw=dict(projection=projection))\n",
    "\n",
    "ax.set_extent([0,360,60,90], ccrs.PlateCarree())\n",
    "ax.add_feature(cfeature.LAND, facecolor='dimgray')\n",
    "#ax.add_feature(cfeature.OCEAN, facecolor='gainsboro')\n",
    "ax.gridlines(draw_labels=True)\n",
    "ax.coastlines(resolution='10m')\n",
    "ax.set_title(var.standard_name, fontsize=20, loc='left')\n",
    "\n",
    "plot = ax.pcolormesh(ds.x, ds.y, var[0,:,:], \n",
    "                     vmin=0,\n",
    "                     vmax=4,\n",
    "                     cmap='Blues')\n",
    "\n",
    "cbar = plt.colorbar(plot, ax=ax, shrink=0.4)\n",
    "\n",
    "fig.savefig('./plots/plot_pre-projected_data_scale_0.98.png', bbox_inches='tight')\n",
    "\n",
    "t2 = time.time()\n",
    "res = t2-t1\n",
    "print(res, 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21dfa0df-6563-4c6e-83bd-e0f772018e1a",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "### 3. Make it faster &#x1F32A;\n",
    "\n",
    "<img src=\"../../images/plot_pre-projected_data_scale_0.98_datashader.png\" alt=\"Pre-projection scale_factor=0.98 datashader\" align=\"right\" width=\"30%\">\n",
    "\n",
    "This computation takes only **10s** using **Datashader's** `dsshow` instead of pcolormesh.\n",
    "\n",
    "<a href=\"https://datashader.org/index.html\"> <b>Datashader</b></a> is a system to create images of large datasets very fast. \n",
    "\n",
    "Unfortunately, there aren't many tutorials on how to use Datashader with geo-referenced data and Matplotlib, but with a little bit of tinkering, the following script comes out taking less than a 10th of the runtime.\n",
    "\n",
    "But of course it should be said that it is considerably faster, since we take a smaller number of x- and y-bins (1400, 1400), which can be specified with _plot_width_ and _plot_height_ in dsshow. Datashader puts the data into the appropriate bins resulting in a significant reduction in the number of data points that end up being drawn. The plot figure size for all plots are always 1400 pixel by 1400 pixel.\n",
    "\n",
    "<!--\n",
    "<img src=\"plot_pre-projected_data_scale_0.98_datashader.png\" alt=\"Pre-projection scale_factor=0.98 datashader\">\n",
    "-->\n",
    "\n",
    "<br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e68df2c-4544-4abb-9879-2580ea59deea",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "import datashader as dsh\n",
    "from datashader.mpl_ext import dsshow"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c02fdda-a27d-4b59-ad4b-010cd8539d6e",
   "metadata": {},
   "source": [
    "First, we create a meshgrid for the used coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22fd7487-ace0-4f25-991c-265ffed6f378",
   "metadata": {},
   "outputs": [],
   "source": [
    "t, y, x = np.meshgrid(ds.time, ds.y, ds.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0689616c-572f-4efb-90ef-a8ad58dbfe2d",
   "metadata": {},
   "source": [
    "Next, we create a Pandas DataFrame containing the variable data and its coordinates. This step is needed to use for datashader's dsshow method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34d95468-7d7e-4322-aafb-0dd8063a45c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = time.time()\n",
    "\n",
    "df = pd.DataFrame({'ALT':ds.ALT[0,:,:].squeeze().values.ravel(), 't':t.flatten(), 'y':y.flatten(), 'x':x.flatten()})#\n",
    "\n",
    "t2 = time.time()\n",
    "print((t2-t1), 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ea15d88-b8ab-452c-9368-3e7e3e754df7",
   "metadata": {},
   "source": [
    "Now, we can generate the plot combining Matplotlib and the datashader artist.\n",
    "\n",
    "<div class=\"alert alert-block alert-danger\">\n",
    "    <b>Note:</b> The ax.set_extent() call has to be moved down below the datashader dsshow call to zoom into the map.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a40d896b-9fef-40e7-bea7-b66ee56f9df5",
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = time.time()\n",
    "\n",
    "projection = ccrs.Stereographic(central_longitude=float(pdict['lon_0']), \n",
    "                                central_latitude=float(pdict['lat_0']),\n",
    "                                false_easting=float(pdict['x_0']),\n",
    "                                false_northing=float(pdict['y_0']), \n",
    "                                scale_factor=0.98,\n",
    "                                globe=globe)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(800*px, 800*px), facecolor='white', subplot_kw={\"projection\": projection})\n",
    "\n",
    "ax.add_feature(cfeature.COASTLINE.with_scale('10m'), linewidth=0.7)\n",
    "ax.add_feature(cfeature.LAND, facecolor='dimgray')\n",
    "ax.gridlines(draw_labels=True)\n",
    "ax.set_title(var.standard_name, fontsize=20, loc='left')\n",
    "\n",
    "artist = dsshow(df,\n",
    "                dsh.Point('x', 'y'),\n",
    "                dsh.mean('ALT'),\n",
    "                vmin=0,\n",
    "                vmax=4,\n",
    "                cmap='Blues',\n",
    "                plot_width=1400,\n",
    "                plot_height=1400,\n",
    "                ax=ax)\n",
    "\n",
    "cbar = plt.colorbar(artist, ax=ax, shrink=0.4)\n",
    "\n",
    "ax.set_extent([0,360,60,90], ccrs.PlateCarree())\n",
    "\n",
    "fig.savefig('./plots/plot_pre-projected_data_scale_0.98_datashader.png', bbox_inches='tight')\n",
    "\n",
    "t2 = time.time()\n",
    "res = t2-t1\n",
    "print(res, 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6b18c39-c48f-4175-a4fc-160448565793",
   "metadata": {},
   "source": [
    "Yes, that is much faster. &#x1F44D;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cc43ac8f-2d4d-4b2d-8656-25de22da83bc",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "datashader",
   "language": "python",
   "name": "datashader"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
