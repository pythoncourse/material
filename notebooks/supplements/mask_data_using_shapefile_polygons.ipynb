{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1b16e626-5107-4ea6-ab88-258f1a8c76ee",
   "metadata": {},
   "source": [
    "# DKRZ Tutorial: &nbsp; &nbsp; How to use a shapefile to mask your data\n",
    "\n",
    "----\n",
    "\n",
    "```\n",
    "Copyright 2022 Deutsches Klimarechenzentrum GmbH (DKRZ)\n",
    "Licensed under CC-BY-NC-SA-4.0\n",
    "```\n",
    "\n",
    "----\n",
    "\n",
    "In the field of data analysis and visualization, in some cases you want to use or display only the data of a certain geographical region. Selecting data of a certain region can be done, among other things, with the help of a shapefile for this region. Shapefiles contain georeferenced points, lines, polygons, and/or area features and can be downloaded from different sites.\n",
    "\n",
    "In this tutorial we will use the topography data generated with CDO as input data. In the first example we extract and draw data for a single country using the countries.shp file and in the second example we use two shapefiles for Germany to mask the data of various German states.\n",
    "\n",
    "**Content**\n",
    "\n",
    "1. Import Python packages\n",
    "1. Example data\n",
    "1. In general\n",
    "   1. Shapefile content\n",
    "   1. Plotting\n",
    "1. Example 1: Extract the data for one state only\n",
    "   1. Generate the mask\n",
    "   1. Mask the data\n",
    "   1. Plotting\n",
    "   1. Set projection (CRS)\n",
    "1. Example 2: Extract the data for German states\n",
    "   1. Generate the mask\n",
    "   1. Mask the data\n",
    "   1. Plotting\n",
    "   1. Get center position of a state\n",
    "   1. Set projection (CRS)\n",
    "   1. Plotting: Add state names and border lines\n",
    "\n",
    "\n",
    "**Shapefile description**\n",
    "\n",
    "From the _ESRI Shapefile Technical Description_ <br>\n",
    "https://www.esri.com/content/dam/esrisites/sitecore-archive/Files/Pdfs/library/whitepapers/pdfs/shapefile.pdf\n",
    "\n",
    "> A shapefile stores nontopological geometry and attribute information for the spatial\n",
    "features in a data set. The geometry for a feature is stored as a shape comprising a set of\n",
    "vector coordinates.\n",
    "> \n",
    "> Because shapefiles do not have the processing overhead of a topological data structure,\n",
    "they have advantages over other data sources such as faster drawing speed and edit\n",
    "ability. Shapefiles handle single features that overlap or that are noncontiguous. They\n",
    "also typically require less disk space and are easier to read and write.\n",
    "> \n",
    "> Shapefiles can support point, line, and area features. Area features are represented as\n",
    "closed loop, double-digitized polygons. Attributes are held in a dBASE® format file.\n",
    "Each attribute record has a one-to-one relationship with the associated shape record.\n",
    "\n",
    "**Shapefiles used**\n",
    "- countries_shp/countries.shp \n",
    "- gadm36_DEU_shp/gadm36_DEU_0.shp\n",
    "- gadm36_DEU_shp/gadm36_DEU_1.shp\n",
    "\n",
    "**Learning content**\n",
    "- Read shapefile content\n",
    "- Extract polygon data of the choosen shapefile content\n",
    "- Mask the variable data with the mask array\n",
    "- Plot the masked data\n",
    "\n",
    "**Shapefile download sites:**\n",
    "- https://sourceforge.net/projects/countriesshp/\n",
    "- https://gadm.org\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "002e7be8-8a3a-4e88-b304-5e22226ce62c",
   "metadata": {},
   "source": [
    "## Import Python packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bbc665ec-0a96-4843-be48-f07274a8edce",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import xarray as xr\n",
    "import numpy as np\n",
    "import geopandas as gpd\n",
    "import pandas as pd\n",
    "import regionmask\n",
    "import matplotlib.pyplot as plt\n",
    "import cartopy.feature as cfeature\n",
    "import cartopy.crs as ccrs\n",
    "\n",
    "from cdo import Cdo\n",
    "cdo = Cdo()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70c39d24-09ef-4809-ba33-5d9bbe50fc61",
   "metadata": {},
   "source": [
    "## Example data\n",
    "\n",
    "The example data used here is the topography data generated with CDO's `topo` operator and a grid resolution of 0.1°x0.1°. Using `returnXDatset=True` and no `output` parameter the result is returned as an Xarray Dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8328244-d72b-4791-9f3d-a125f1978de9",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_topo = cdo.remapbil('global_0.1', input='-topo', returnXDataset=True)\n",
    "ds_topo"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "891441ef-64e3-4702-a664-bdb0ca24eb37",
   "metadata": {},
   "source": [
    "Use variable _topo_ from the Dataset and display the example data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea0eb929-d6c9-4047-9a76-f1028f1dbfcb",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(8,4), subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "\n",
    "ax.coastlines(linewidth=0.5)\n",
    "\n",
    "var = ds_topo['topo']\n",
    "\n",
    "ax.pcolormesh(var.lon, var.lat, var, \n",
    "              cmap='RdBu_r', \n",
    "              transform=ccrs.PlateCarree());\n",
    "\n",
    "#var.plot(add_colorbar=False, \n",
    "#         transform=ccrs.PlateCarree(), \n",
    "#         subplot_kws={'projection': ccrs.PlateCarree()});"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93479e8d-4732-45c7-8c0e-b65638a1993b",
   "metadata": {},
   "source": [
    "## In general\n",
    "\n",
    "### Shapefile content\n",
    "\n",
    "GeoPandas provide the function geopandas.read_file() that reads among others a shapefile and returns a GeoDataFrame which is similar to a Pandas Dataframe.\n",
    "\n",
    "The tar file containing the shapefile can be downloaded from https://nextcloud.dkrz.de/s/fyBbtYBz4M3BaEe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5177aa5-574e-411b-89f4-3ba202965eb3",
   "metadata": {},
   "outputs": [],
   "source": [
    "shp_file = os.environ['HOME']+'/Downloads/countries_shp/countries.shp'\n",
    "\n",
    "content = gpd.read_file(shp_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3535d073-ee39-4aed-a15e-24ba8d78e2a3",
   "metadata": {},
   "source": [
    "Print first few rows of the GeoDataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b170dbc-0597-4c08-a87b-5d3ed214aaea",
   "metadata": {},
   "outputs": [],
   "source": [
    "content.head()      # print only first 5 rows\n",
    "\n",
    "#display(content)    # print all"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f55e741-e455-49dd-9945-cd7523e23809",
   "metadata": {},
   "source": [
    "### Plotting\n",
    "\n",
    "First, let's see what kind of CRS (Coordinate Reference System) is set for _content_. The attribute `crs` of the GeoDataFRame returns the CRS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c89ba5f-4fa5-475c-8a4b-ec7e8673c85a",
   "metadata": {},
   "outputs": [],
   "source": [
    "content.crs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6d3053f-c771-4152-a082-b37276335643",
   "metadata": {},
   "source": [
    "The returned content variable is a GeoDataframe which can be plotted using the GeoPandas plot() method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "013ad0ca-17c6-4878-8745-0ad3b9d26ede",
   "metadata": {},
   "outputs": [],
   "source": [
    "content.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c752b299-35ef-4226-ac58-ea0fd0c0a3ff",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Example 1: Extract the data for one state only\n",
    "\n",
    "Let's start from the beginning and read the shapefile content into a GeoDataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd692017-68a9-4342-969a-12f3e19c9943",
   "metadata": {},
   "outputs": [],
   "source": [
    "content = gpd.read_file(shp_file)\n",
    "\n",
    "type(content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61deaec2-586a-4809-9d7d-c93822085a91",
   "metadata": {},
   "source": [
    "The country names are stored in the column _NAME_ of the GeoDataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "129b539c-df33-435a-aee0-cd59ea3afeed",
   "metadata": {},
   "outputs": [],
   "source": [
    "shp_var = 'NAME'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e132b0c-5b6d-425d-8dbd-9ffb79aa7d7c",
   "metadata": {},
   "source": [
    "Get the list of all countries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37e85c20-7440-4f20-ab1a-b4df7390dd3b",
   "metadata": {},
   "outputs": [],
   "source": [
    "country_names = [ x for x in content[shp_var]]\n",
    "#print(country_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e47f7c3-a1e3-4215-b637-0da2cfc895ef",
   "metadata": {},
   "source": [
    "We want to use only one country therefore we assign a variable _name_ that keeps the chosen country name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30246968-dade-4007-a85f-5a95411980fd",
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'Canada'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fdeef509-7e3a-49b0-add2-949d3c81495a",
   "metadata": {},
   "source": [
    "Check if the chosen country is in the GeoDataFrame (read from the shapefile), check if it is a substring (e.g. 'Republic') and would fit to multiple countries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d63eb902-d840-4d68-963a-90f4472747ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "ncheck = [n for n in country_names if name in n]\n",
    "\n",
    "if ncheck == []:\n",
    "    raise NameError(f'ERROR: \"{name}\" not found in shapefile')\n",
    "elif len(ncheck) > 1:\n",
    "    raise NameError(f'ERROR: found multiple entries for \"{name}\" in shapefile\\n                  {ncheck}')\n",
    "else:\n",
    "    print(ncheck)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce4ce049-7873-419c-be6b-6dac999284df",
   "metadata": {},
   "source": [
    "Extract the data of the chosen country from the GeoDataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f31b6d78-3988-4025-a8b7-b50eb06b8e90",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = content.loc[content['NAME'] == name]\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c042b8aa-9c9f-4122-8e19-b2370322efde",
   "metadata": {},
   "source": [
    "Have a look at the extracted country."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "92211486-78cb-4ecc-83e6-98f58d6cd251",
   "metadata": {},
   "outputs": [],
   "source": [
    "df.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a23a97f7-3994-4108-a0fa-ccf360780ea1",
   "metadata": {},
   "source": [
    "### Plotting\n",
    "\n",
    "In the next step, we draw the multipolygon of the selected country on top of the example _topo_ plot.\n",
    "\n",
    "Using both, Matplotlib and the plotting features of geopandas in same figure, we have to make sure that the `aspect='equal'` is set in the geopandas plot call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "775924dd-a39e-47ad-b6ed-a9511a988597",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(8,4), subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "\n",
    "ax.coastlines(linewidth=0.5)\n",
    "\n",
    "plot = ax.pcolormesh(var.lon, var.lat, var, \n",
    "                     cmap='RdBu_r', \n",
    "                     vmin=-10000, \n",
    "                     vmax=10000, \n",
    "                     transform=ccrs.PlateCarree(), \n",
    "                     zorder=0)\n",
    "\n",
    "df.plot(ax=ax, color='gray', transform=ccrs.PlateCarree(), aspect='equal', zorder=1)\n",
    "\n",
    "plt.colorbar(plot, shrink=0.6)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b9c9387-cc10-4408-ba21-9ea4a114ed6c",
   "metadata": {},
   "source": [
    "### Set projection (CRS)\n",
    "\n",
    "We can convert the GeoDataFrame to a given projection, here ccrs.PlateCarree(), with Cartopy's `.to_crs()` method using the  Proj4 projection string generated with the method `.proj4_init` from the chosen projection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9cb20415-b1d1-4d21-8f5e-7bcb42a1645b",
   "metadata": {},
   "outputs": [],
   "source": [
    "CRS       = ccrs.PlateCarree()\n",
    "#crs_proj4 = CRS.proj4_init\n",
    "df_pc     = df.to_crs(CRS.proj4_init)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(8,4), subplot_kw=dict(projection=CRS))\n",
    "\n",
    "ax.coastlines(linewidth=0.5)\n",
    "\n",
    "plot = ax.pcolormesh(var.lon, var.lat, var, \n",
    "                     cmap='RdBu_r', \n",
    "                     vmin=-10000, \n",
    "                     vmax=10000, \n",
    "                     transform=CRS, \n",
    "                     zorder=0)\n",
    "\n",
    "df_pc.plot(ax=ax, color='gray', zorder=1)\n",
    "\n",
    "plt.colorbar(plot, shrink=0.6)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2c63bc2-8310-4cdd-ba22-cb4dd1b91323",
   "metadata": {},
   "source": [
    "### Generate the mask\n",
    "\n",
    "The `regionmask.mask_geopandas()` method needs the GeoDataFrame of the selected shapefile, longitude and latitude (must be 'lon' and 'lat'). Lon and lat can be generated using np.arange() or here we use the lon and lat arrays of the example Dataset _ds_topo_ to be able to use the `xarray.where()` method later to extract the data for the country."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6763c1ab-f381-40d4-b4b5-452939a185e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = regionmask.mask_geopandas(df, ds_topo.lon, ds_topo.lat)\n",
    "mask"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "336cac5b-cff3-4bd0-9231-cee06623a056",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.unique(mask)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "feeacb8d-14df-4fe4-858a-59ae3e034c0a",
   "metadata": {},
   "source": [
    "**Note:** Instead of getting a mask containing zero and one we get a mask containing NaN and the index number of the country in the GeoDataFrame, e.g. 43 for Canada. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1eb8822b-e109-42d0-8a73-4c3a519d2dde",
   "metadata": {},
   "source": [
    "Plot the mask."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b63d6f4-78ec-43ae-afb5-4a0a57e5ccd3",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "\n",
    "mask.plot(ax=ax, \n",
    "          transform=ccrs.PlateCarree(),\n",
    "          add_colorbar=False)\n",
    "\n",
    "ax.coastlines();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3df62e0c-0b65-4816-91a3-fd22ee5596c9",
   "metadata": {},
   "source": [
    "### Mask the data\n",
    "\n",
    "Now we can use the mask array to extract the values of the variable _topo_ when mask is equal the index of the country (index 43 = Canada)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95fd45c5-cbda-4e86-ac95-8ddbe0e0d767",
   "metadata": {},
   "outputs": [],
   "source": [
    "country_index = df.index.values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "68da0b20-6169-4f46-8e60-f1fb85422f5a",
   "metadata": {},
   "outputs": [],
   "source": [
    "masked_data = var.where(mask == country_index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97de96f3-fe8b-4d4f-9eba-b85c3f81d915",
   "metadata": {},
   "outputs": [],
   "source": [
    "masked_data.max()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a94ed31-ccc5-465a-9eba-e7b8126896c1",
   "metadata": {},
   "source": [
    "Plot the masked data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d29b536d-61d5-4c30-abac-cd3fc6cca205",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "\n",
    "ax.pcolormesh(masked_data.lon, masked_data.lat, masked_data,\n",
    "              cmap='Reds', \n",
    "              vmin=0, \n",
    "              vmax= 3000, \n",
    "              transform=ccrs.PlateCarree())\n",
    "\n",
    "ax.coastlines(linewidth=0.5);\n",
    "ax.add_feature(cfeature.BORDERS, linewidth=0.5)\n",
    "ax.add_feature(cfeature.OCEAN)\n",
    "ax.add_feature(cfeature.LAND, color='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22481b4c-0c90-49a4-b401-edf295e9c08c",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "## Example 2: Extract the data for German states\n",
    "\n",
    "This time we use two shapefiles named **gadm36_DEU_0.shp** and **gadm36_DEU_1.shp** that contain the border polygons of Germany and its states.\n",
    "\n",
    "Read the first shapefile containing the German border polygons.\n",
    "\n",
    "The tar file containing the shapefile can be downloaded from https://nextcloud.dkrz.de/s/XB7fpgkrHoSEyKe\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "69e4debc-2be3-47e3-b817-ee1496dfbfd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "shp_file0 = os.environ['HOME']+'/Downloads/gadm36_DEU_shp/gadm36_DEU_0.shp'\n",
    "\n",
    "content0 = gpd.read_file(shp_file0)\n",
    "\n",
    "content0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a3da28a-7d9c-40d6-b50f-19cef1aa943e",
   "metadata": {},
   "source": [
    "Plot the GeoDataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1cf0f50c-4e31-40a6-afa5-c08bcef062a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "df0 = content0\n",
    "df0.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8721ddc-5f10-42bf-8cb5-f6dc12b3ba7b",
   "metadata": {},
   "source": [
    "Read the second shapefile containing the border polygons of the German states. The names of the German states are stored in the column _NAME_1_."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95557da6-aa06-46f9-aac9-7e8010ac91f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "shp_file1 = os.environ['HOME']+'/Downloads/gadm36_DEU_shp/gadm36_DEU_1.shp'\n",
    "\n",
    "content1 = gpd.read_file(shp_file1)\n",
    "\n",
    "shp_var1 = 'NAME_1'\n",
    "states = ['Bayern', 'Baden-Württemberg', 'Rheinland-Pfalz', 'Saarland']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78eea6dc-9c8d-48ee-a6ca-eed688e7877d",
   "metadata": {},
   "source": [
    "Check if the choosen states are in the shapefile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb956c46-35f2-4e24-b59a-44b94dcc8b35",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get list of all states in shapefile.\n",
    "state_names = [ x for x in content1[shp_var1] ]\n",
    "\n",
    "# Check if states is in the shapefile.\n",
    "ncheck = [n for n in state_names if n in states]\n",
    "\n",
    "if ncheck == []:\n",
    "    raise NameError(f'ERROR: \"{states}\" not found in shapefile')\n",
    "else:\n",
    "    print(ncheck)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3fd71f77-1e29-4416-b807-6b1d092bf3b9",
   "metadata": {},
   "source": [
    "Extract the pyolgons for the states from the shapefile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c1988a4-6869-4587-8c35-736582e0e4eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "df1 = content1[content1[shp_var1].isin(states)]\n",
    "df1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7bd6ded0-de47-4331-9f97-3fbc2e6c9e7c",
   "metadata": {},
   "source": [
    "### Generate the mask"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c74df0f8-a5b9-4443-ad51-9558ec578234",
   "metadata": {},
   "outputs": [],
   "source": [
    "mask1 = regionmask.mask_geopandas(df1, ds_topo.lon, ds_topo.lat)\n",
    "mask_id = np.unique(mask1)[0:-1]\n",
    "mask_id"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e18964fb-cb35-4c7b-b06b-981c0b30c96c",
   "metadata": {},
   "outputs": [],
   "source": [
    "masked_data1 = var.where(mask1.isin(mask_id))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fdf46cf3-b8c3-420a-bb8d-81bcfa8e4206",
   "metadata": {},
   "source": [
    "Plot the chosen German states."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06aee463-cbb8-464f-87e5-c230e4ce32f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(10,10),subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "\n",
    "ax.set_extent([5.0, 16.0, 47.0, 56.0], crs=ccrs.PlateCarree())\n",
    "ax.gridlines(draw_labels=True)\n",
    "ax.add_feature(cfeature.OCEAN)\n",
    "ax.add_feature(cfeature.LAND)\n",
    "ax.coastlines(linewidth=0.5)\n",
    "\n",
    "# draw Germany in white\n",
    "df0.plot(ax=ax, color='white', linewidth=0.8, zorder=0)\n",
    "\n",
    "# draw German border line\n",
    "df0.boundary.plot(ax=ax, color='black', linewidth=0.8, zorder=1)\n",
    "\n",
    "# draw masked data of chosen states\n",
    "masked_data1.plot(ax=ax, cmap='RdYlBu_r', transform=ccrs.PlateCarree(),zorder=2)\n",
    "\n",
    "# draw state border lines of chosen states\n",
    "df1.boundary.plot(ax=ax, color='black', linewidth=1., zorder=3);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7340e8f1-cef4-4e38-a2bc-8b839c91f2dd",
   "metadata": {},
   "source": [
    "### Set projection (CRS)\n",
    "\n",
    "Next, we want to get rid of the 'UserWarning: Geometry is in a geographic CRS.' again we set the projection to ccrs.PlateCarree()."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2553fb40-9a3f-417b-a264-ee171f42dfdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "crs       = ccrs.PlateCarree()\n",
    "crs_proj4 = crs.proj4_init\n",
    "df1_pc    = df1.to_crs(crs_proj4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a88025cb-06a7-4345-b97a-7c0eee38759d",
   "metadata": {},
   "source": [
    "### Get center position of a state\n",
    "\n",
    "To add the state name at the center of each state you can use the `GeoDataFrame.centroid` method to retrieve the x- and y-value of their center position."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db99102a-a2c3-4991-9c03-8205aa580ba5",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = df1_pc.centroid.map(lambda p: p.x)\n",
    "y = df1_pc.centroid.map(lambda p: p.y)\n",
    "\n",
    "print(x.values)\n",
    "print(y.values)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ca19a55-0aaa-4b62-8054-86c8b593fce6",
   "metadata": {},
   "source": [
    "### Plotting: Add state names and border lines\n",
    "\n",
    "Now we can add the state names to the plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8cb32098-e035-422f-bbc0-0a5956e2e390",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(10,10),subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "\n",
    "ax.set_extent([5.0, 16.0, 47.0, 56.0], crs=ccrs.PlateCarree())\n",
    "ax.gridlines(draw_labels=True)\n",
    "ax.add_feature(cfeature.OCEAN)\n",
    "ax.add_feature(cfeature.LAND)\n",
    "ax.coastlines(linewidth=0.5)\n",
    "\n",
    "# draw Germany in white\n",
    "df0.plot(ax=ax, color='white', linewidth=0.8, zorder=0)\n",
    "\n",
    "#draw German border line\n",
    "df0.boundary.plot(ax=ax, color='black', linewidth=0.8, zorder=1)\n",
    "\n",
    "# draw masked data of chosen states\n",
    "masked_data1.plot(ax=ax, cmap='RdYlBu_r', transform=ccrs.PlateCarree(),\n",
    "                  cbar_kwargs={'orientation':'vertical', 'shrink':0.6, 'pad':0.1},\n",
    "                  zorder=2)\n",
    "\n",
    "# draw state border lines\n",
    "content1.boundary.plot(ax=ax, color='black', linewidth=1., transform=crs, zorder=3)\n",
    "\n",
    "# add chosen state names\n",
    "for i in df1_pc.index.values:\n",
    "    ax.text(x[i], y[i], df1[\"NAME_1\"][i], ha='center', fontsize=7)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42d712c2-a006-4d3a-9c4d-ed306f237d6b",
   "metadata": {},
   "source": [
    "Have a short look at the last gadm36_DEU shapefile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79573476-0385-4c9e-a905-d6b4de9f114b",
   "metadata": {},
   "outputs": [],
   "source": [
    "shp_file2 = os.environ['HOME']+'/Downloads/gadm36_DEU_shp/gadm36_DEU_2.shp'\n",
    "\n",
    "content2 = gpd.read_file(shp_file2)\n",
    "\n",
    "content2.plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd810f7a-f4e8-45fd-ad09-119ba53983d8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "cartopy",
   "language": "python",
   "name": "cartopy"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
