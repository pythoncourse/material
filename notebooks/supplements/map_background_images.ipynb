{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "cacd9f50-37dd-4d51-ae0c-d14ece578dfc",
   "metadata": {},
   "source": [
    "# DKRZ Python visualization\n",
    "\n",
    "## Use an image for the map background\n",
    "\n",
    "<img src=\"../../images/plot_tos_with_background_image.png\" alt=\"../../images/plot_tos_with_background_image.png\" width=\"50%\">\n",
    "\n",
    "----\n",
    "\n",
    "```\n",
    "Copyright 2022 Deutsches Klimarechenzentrum GmbH (DKRZ)\n",
    "Licensed under CC-BY-NC-SA-4.0\n",
    "```\n",
    "\n",
    "----\n",
    "\n",
    "**Content**\n",
    "- Background images\n",
    "- Combine background image and data\n",
    "- Zoom into the map\n",
    "- Cartopy image tiles\n",
    "\n",
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d23bea1-1446-453e-818f-003d8d92fda9",
   "metadata": {},
   "source": [
    "<a href=\"https://scitools.org.uk/cartopy/docs/latest/\"> Cartopy </a> offers the possibility to add an image to the plot, e.g. the above plot of global 'sea surface temperature' data is backed with a satellite image of the earth.\n",
    "\n",
    "In this example we show how to create such a plot and give some background information on the use of background images."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4a12edb-6cac-4b66-a5e1-2a540e96fe21",
   "metadata": {},
   "source": [
    "## Cartopy - background images\n",
    "\n",
    "First, we have a look at the <a href=\"https://neo.gsfc.nasa.gov/view.php?datasetId=BlueMarbleNG&date=2004-08-01\"> NASA Earth Observation download page \n",
    "</a> to get the Earth satellite image with a high resolution of 0.1 degrees (3600x1800 pixels).\n",
    "\n",
    "The images have to be stored in a directory together with a JSON file named\n",
    "`images.json` that will be read by Cartopy's \n",
    "<a href=\"https://scitools.org.uk/cartopy/docs/latest/reference/generated/cartopy.mpl.geoaxes.GeoAxes.html?highlight=background_img#cartopy.mpl.geoaxes.GeoAxes.background_img\"> cartopy.mpl.geoaxes.GeoAxes.background_image() </a>  method.\n",
    "\n",
    "Cartopy searchs the background images in the directory specified by the environment variable `CARTOPY_USER_BACKGROUND`.\n",
    "\n",
    "Example `images.json` file:\n",
    "\n",
    "```\n",
    "{\"__comment__\": \"JSON file specifying background images. env CARTOPY_USER_BACKGROUNDS, ax.background_img()\",\n",
    "  \"BlueMarble\": {\n",
    "    \"__comment__\": \"Blue Marble Next Generation, Aug 2004, 0.1 degrees\",\n",
    "    \"__source__\": \"https://neo.gsfc.nasa.gov/servlet/RenderData?si=526300&cs=rgb&format=JPEG&width=3600&height=1800\",\n",
    "    \"__projection__\": \"PlateCarree\",\n",
    "    \"high\": \"BlueMarble_3600x1800.png\"\n",
    "  },\n",
    "  \n",
    "  \"BlueMarbleBright\": {\n",
    "    \"__comment__\": \"Blue Marble Next Generation, Aug 2004, 0.1 degrees, brighter\",\n",
    "    \"__source__\": \"https://neo.gsfc.nasa.gov/servlet/RenderData?si=526300&cs=rgb&format=JPEG&width=3600&height=1800\",\n",
    "    \"__projection__\": \"PlateCarree\",\n",
    "    \"high\": \"BlueMarble_3600x1800_bright.png\"\n",
    "  },\n",
    "  \n",
    "    \"NaturalEarthRelief\": {\n",
    "    \"__comment__\": \"Natural Earth I with shaded Relief, water, and drainage\",\n",
    "    \"__source__\": \"https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/raster/NE1_HR_LC_SR_W_DR.zip\",\n",
    "    \"__projection__\": \"PlateCarree\",\n",
    "    \"high\": \"NE1_HR_LC_SR_W_DR.png\"\n",
    "  },\n",
    " \n",
    "  \"GrayEarth\": {\n",
    "    \"__comment__\": \"Gray Earth with shaded relief, hypsography and ocean bottom, high res\",\n",
    "    \"__source__\": \"https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/raster/GRAY_HR_SR_OB.zip\",\n",
    "    \"__projection__\": \"PlateCarree\",\n",
    "    \"high\": \"GRAY_HR_SR_OB_high_res.png\"\n",
    "  }\n",
    "}\n",
    "```\n",
    "\n",
    "<div class=\"alert alert-block alert-danger\">\n",
    "    <b>Note:</b> <br>\n",
    "    The image file <em>NE1_HR_LC_SR_W_DR.tif</em> from the zip file\n",
    "    <em>NE1_HR_LC_SR_W_DR.zip</em> has to be converted to PNG file format.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7cdbcecd-0270-4d13-9921-4584c9b330f4",
   "metadata": {},
   "source": [
    "### Import packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bdc1e384-0d29-4a36-b618-a9679d263625",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import xarray as xr\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import cartopy.crs as ccrs\n",
    "import cartopy.feature as cfeature\n",
    "\n",
    "from cmocean import cm as cmo"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd10eaab-619f-4296-afa1-30ceac20356f",
   "metadata": {},
   "source": [
    "### Background images\n",
    "\n",
    "To be able to use an satellite Earth image as background in a map plot, we have to create an `images.json` file that will be read by Cartopy. Therefore, I've created an folder $HOME/Python/backgrounds where I stored the json file from below. \n",
    "\n",
    "\n",
    "The environment variable CARTOPY_USER_BACKGROUNDS can then be set via the `os.environ` function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8648c3e9-3c4b-4880-a558-d8916c668401",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ['CARTOPY_USER_BACKGROUNDS'] = os.environ['HOME']+'/Python/backgrounds'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c52b6d89-0e40-4e59-92c3-76b9caeb1def",
   "metadata": {},
   "source": [
    "Let's have a look at the `$HOME/Python/backgrounds/images.json` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90821998-d739-4af4-878b-368e0da9366a",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat $CARTOPY_USER_BACKGROUNDS/images.json"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20434edd-4c66-44ee-a029-b46fe5b8c460",
   "metadata": {},
   "source": [
    "We choose the *BlueMarbleBright*  image and have a short view at the image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "361ad347-18fe-4274-b490-13842eb4b42c",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection=ccrs.PlateCarree()))\n",
    "bg = ax.background_img(name='BlueMarbleBright', resolution='high')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "325cd0c6-4983-4a95-a33a-043023ac8c71",
   "metadata": {},
   "source": [
    "### Combine background image and data\n",
    "\n",
    "In this example we use the CDO topography data set to generate a dataset where land masses are NaNs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa7abdb7-fc9e-4983-acc7-ee6bed7201f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "from cdo import Cdo\n",
    "cdo = Cdo()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e34ca2fa-912e-4147-bce7-b2fea6ca335d",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = cdo.expr('\"topo = ((topo < 0.0)) ? topo :(topo/0.0)\"', \n",
    "              input='-topo ', \n",
    "              options='-f nc', \n",
    "              returnXDataset=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd4f7123-36f8-4b6a-8c9c-1fa9e9e1957a",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a452b5a1-9df3-490f-a558-6358ee6fd5bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "topo = ds.topo\n",
    "lon = ds.lon\n",
    "lat = ds.lat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27e9a700-7bad-4583-bb64-53f2b728d41c",
   "metadata": {},
   "source": [
    "Create the plot of the global data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ec533ef-c91f-4313-9faf-96c5dd9cec0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "proj = ccrs.PlateCarree()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection=proj))\n",
    "\n",
    "ax.coastlines()\n",
    "gl = ax.gridlines(draw_labels=True)\n",
    "gl.xlines = False\n",
    "gl.ylines = False\n",
    "\n",
    "ax.background_img(name='BlueMarbleBright', resolution='high')\n",
    "\n",
    "plot = ax.pcolormesh(lon, lat, topo,\n",
    "                     cmap=cmo.balance,\n",
    "                     vmin=-10000,\n",
    "                     vmax=10000,\n",
    "                     transform=ccrs.PlateCarree())\n",
    "cbar = plt.colorbar(plot, pad=0.065, shrink=0.4)\n",
    "\n",
    "#fig.savefig('./plots/plot_tos_with_background_image.png', bbox_inches='tight')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ef8fc20-7320-486c-a758-2e8e3559e17e",
   "metadata": {},
   "source": [
    "### Zoom into the map"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7578902f-7588-4ce7-af33-59d6214f6a5c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22d4c73b-b0fa-4caa-8dc1-907ce9f0a247",
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = time.time()\n",
    "\n",
    "proj = ccrs.PlateCarree()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection=proj))\n",
    "\n",
    "ax.set_extent([-15.0, 45.0, 25.0, 45.0])\n",
    "ax.coastlines()\n",
    "gl = ax.gridlines(draw_labels=True)\n",
    "gl.xlines = False\n",
    "gl.ylines = False\n",
    "\n",
    "ax.background_img(name='BlueMarbleBright', resolution='high')\n",
    "\n",
    "plot = ax.pcolormesh(lon, lat, topo,\n",
    "                     cmap=cmo.balance,\n",
    "                     transform=ccrs.PlateCarree())\n",
    "cbar = plt.colorbar(plot, pad=0.065, shrink=0.4)\n",
    "\n",
    "#fig.savefig('./plots/plot_tos_with_background_image_area1.png', bbox_inches='tight')\n",
    "\n",
    "t2 = time.time()\n",
    "print('Run time: ', (t2-t1), 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c062a639-6960-4d89-93d5-366af8f17ca3",
   "metadata": {},
   "source": [
    "If you are using a high resolution global image than zooming into a sub-region will take a long time caused by the rendering of the image. Therefore the background_img method provides a parameter `extent`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e215d2b-ec9a-4804-b3e9-cdd00040281b",
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = time.time()\n",
    "\n",
    "proj = ccrs.PlateCarree()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection=proj))\n",
    "\n",
    "ax.set_extent([-15.0, 45.0, 25.0, 45.0])\n",
    "ax.coastlines()\n",
    "gl = ax.gridlines(draw_labels=True)\n",
    "gl.xlines = False\n",
    "gl.ylines = False\n",
    "\n",
    "ax.background_img(name='BlueMarbleBright', extent=[-15.0, 45.0, 25.0, 48.6], resolution='high')\n",
    "\n",
    "plot = ax.pcolormesh(lon, lat, topo,\n",
    "                     cmap=cmo.balance,\n",
    "                     transform=ccrs.PlateCarree())\n",
    "cbar = plt.colorbar(plot, pad=0.065, shrink=0.4)\n",
    "\n",
    "#fig.savefig('./plots/plot_tos_with_background_image_area1.png', bbox_inches='tight')\n",
    "\n",
    "t2 = time.time()\n",
    "print('Run time: ', (t2-t1), 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e9667df-feaa-4eb1-b4f0-14e8c6e28cc6",
   "metadata": {},
   "source": [
    "Next, we will have a look at the *GrayEarth* image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4147f0ad-eb0b-4029-8dd7-ca57ba64a03f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import cartopy.feature as cfeature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8286518-a0c3-4947-8999-fdae223795c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "proj = ccrs.PlateCarree()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection=proj))\n",
    "\n",
    "ax.set_extent([3.0, 25.0, 53.0, 72.])\n",
    "ax.coastlines(resolution='10m')\n",
    "ax.add_feature(cfeature.LAKES, facecolor='None', edgecolor='k')\n",
    "ax.background_img(name='GrayEarth', extent=[3.0, 25.0, 53.0, 72.], resolution='high')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0993d8e5-6bf8-46c2-a71a-9fe27657d405",
   "metadata": {},
   "source": [
    "Another nice image for the background is the *NaturalEarthRelief* image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2bdb2b0-11ba-4109-b40a-d05e39bc63f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "proj = ccrs.PlateCarree()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10,10), subplot_kw=dict(projection=proj))\n",
    "\n",
    "ax.set_extent([3.0, 25.0, 53.0, 72.])\n",
    "ax.coastlines(resolution='10m', linewidth=0.5)\n",
    "ax.background_img(name='NaturalEarthRelief', extent=[3.0, 25.0, 53.0, 72.], resolution='high')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a79262e0-eb15-47e3-aa78-fb879703119e",
   "metadata": {},
   "source": [
    "## Cartopys image tiles\n",
    "\n",
    "The next example shows how to show an Open Street Map (OSM) for a given area.\n",
    "\n",
    "See https://scitools.org.uk/cartopy/docs/latest/reference/generated/cartopy.io.img_tiles.OSM.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da73f823-0fdf-4215-8c38-e6030a8f1f9f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import cartopy.io.img_tiles as cimgt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3fbba0b3-22f9-4b91-8e3f-fb72d3060053",
   "metadata": {},
   "source": [
    "We can use the projection of the chosen tiles, here Open-Street-Map (OSM), to create the map plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "636ea458-7591-48e0-b292-f461c48d0efb",
   "metadata": {},
   "outputs": [],
   "source": [
    "osm_tiles = cimgt.OSM()\n",
    "\n",
    "plt.figure(figsize=(10, 10))\n",
    "\n",
    "# Use the tile's projection for the underlying map.\n",
    "ax = plt.axes(projection=osm_tiles.crs)\n",
    "\n",
    "# Specify a region of interest, in this case, Cardiff.\n",
    "ax.set_extent([9.25, 10.75, 53., 54.],\n",
    "              ccrs.PlateCarree())\n",
    "\n",
    "# Add the tiles at zoom level 12.\n",
    "ax.add_image(osm_tiles, 10)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "145749c0-5a9d-4554-a5ca-21c45ae69454",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "cartopy",
   "language": "python",
   "name": "cartopy"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
