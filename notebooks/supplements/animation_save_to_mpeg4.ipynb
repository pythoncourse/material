{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7083ed2e-7f90-4420-9956-627b9a0dd126",
   "metadata": {},
   "source": [
    "# DKRZ Python visualization\n",
    "\n",
    "## Save animation to MPEG-4 file\n",
    "\n",
    "----\n",
    "\n",
    "```\n",
    "Copyright 2023 Deutsches Klimarechenzentrum GmbH (DKRZ)\n",
    "Licensed under CC-BY-NC-SA-4.0\n",
    "```\n",
    "\n",
    "----\n",
    "\n",
    "This notebook will briefly demonstrate how to create an animation of time series data using Matplotlib, which will then be saved as an MPEG-4 file. \n",
    "\n",
    "It uses the 'air_temperature' data from the **Xarray tutorial dataset**, so anyone can run this script if **Xarray** is installed. Also, the **ffmpeg** package has to be installed.\n",
    "\n",
    "**Content**\n",
    "- Example data\n",
    "- Create Animation\n",
    "  - Two ways to go\n",
    "    - Use IPython's display\n",
    "    - Use rcParams 'jshtml'\n",
    "- Save the animation to MPEG-4\n",
    "\n",
    "<br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e6dad765-a62f-4430-b8ce-399c2827491b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adbce395-fb53-4be0-a6af-2b6a5c44972e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import xarray as xr\n",
    "\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.animation as anim\n",
    "import cartopy.crs as ccrs\n",
    "\n",
    "from IPython.display import HTML"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96ffcb89-d92d-4c18-a6ce-def375ddb1c1",
   "metadata": {},
   "source": [
    "## Example data\n",
    "\n",
    "The example data is taken from the Xarray tutorial dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "584aaba1-a5a8-4657-8d64-11d296db94c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.tutorial.load_dataset(\"air_temperature\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "755a2efe-a0a6-4ad2-9bce-ee33d614f327",
   "metadata": {},
   "source": [
    "The data is stored every 6 hours but we want to display in our animation the daily means."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6626a907-71eb-4866-a02b-7e66cf1e67a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = ds.resample(time='1D').mean(dim='time')\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41f40a2d-0c42-4bfb-b03c-0124b7929a4a",
   "metadata": {},
   "source": [
    "Extract the data for the first 6 months."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "04bc8ac7-32fe-4fb9-bedf-242041fe7c19",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = ds.sel(time=slice('2013-01-01','2013-06-30'))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e85f7ffa-989b-467f-82c4-d091f2c1e13b",
   "metadata": {},
   "source": [
    "Get the coordinates, time and the variable 'air' data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3fbb3a7-434e-4753-abc5-856711620061",
   "metadata": {},
   "outputs": [],
   "source": [
    "lon = ds.lon\n",
    "lat = ds.lat\n",
    "time = ds.time\n",
    "var = ds.air"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1cd03c7-bcae-4536-b804-17631d302ac5",
   "metadata": {},
   "source": [
    "## Create Animation\n",
    "\n",
    "### Two ways to go\n",
    "\n",
    "The generation of the animation object is very similar for the following two variants shown.\n",
    "\n",
    "We generate the animation in a cell **without** displaying it using the built-in magics command `%%capture` that let us run the cell, capturing stdout, stderr, and IPython’s rich display() calls. \n",
    "\n",
    "Because the maximum default size of an animation is too small for our animation, we increase the value for the corresponding Matplotlib resource parameter `animation.embeded_limit`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7efe4f9-4f6d-48ff-b4ec-45087573d751",
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams['animation.embed_limit'] = 2**128"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c076f22-3020-4609-bc32-407f0fca948d",
   "metadata": {},
   "source": [
    "### A. Use IPython's display"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8bd0e99-8a32-4add-954c-ff709276e3da",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "fig, ax = plt.subplots(figsize=(10,5), subplot_kw={\"projection\": ccrs.PlateCarree()})\n",
    "\n",
    "ax.coastlines()\n",
    "ax.gridlines(draw_labels=True)\n",
    "\n",
    "date_label = ax.text(0.985, 1.1, f'{ds.time[0].dt.strftime(\"%Y-%m-%d\").data}', \n",
    "                     ha='right', fontname='Courier New',\n",
    "                     transform=ax.transAxes, \n",
    "                     bbox=dict(boxstyle='round', facecolor='whitesmoke'))\n",
    "\n",
    "levels = 20          # number of contour levels\n",
    "cmap = 'RdYlBu_r'    # choose colormap\n",
    "\n",
    "p = [ ax.contourf(lon, lat, var[0,:,:], levels=levels, cmap=cmap ) ]\n",
    "\n",
    "def update(i):\n",
    "    for tp in p[0].collections:\n",
    "        tp.remove()\n",
    "    date_label.set_text(f'{ds.time[i].dt.strftime(\"%Y-%m-%d\").data}')\n",
    "    p[0] = ax.contourf(lon, lat, var[i,:,:], levels=levels, cmap=cmap ) \n",
    "    return p[0].collections+[date_label]\n",
    "\n",
    "delay = 200     # delay between frames in milliseconds\n",
    "\n",
    "ani = anim.FuncAnimation(fig, update, frames=len(ds.time), interval=delay, \n",
    "                              blit=True, repeat=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "729ddac4-0479-4589-9e63-c0d7d2b287dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "display(HTML(ani.to_jshtml()))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5bfc3fe-c329-4ef8-8b59-3977fe701188",
   "metadata": {},
   "source": [
    "### B. Use rcParams 'jshtml'\n",
    "\n",
    "In the second variant we do not use the display function of IPython, we call the animation object directly. Therefore, we have to tell Matplotlib which kind of animation has to be used. This is done with \n",
    "\n",
    "    plt.rcParams['animation.html'] = 'jshtml'\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d6b83cb-94f1-4b15-9633-e9e4c69bc4e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "fig, ax = plt.subplots(figsize=(10,5), subplot_kw={\"projection\": ccrs.PlateCarree()})\n",
    "\n",
    "plt.rcParams['animation.html'] = 'jshtml'\n",
    "\n",
    "ax.coastlines()\n",
    "ax.gridlines(draw_labels=True)\n",
    "\n",
    "date_label = ax.text(0.985, 1.1, f'{ds.time[0].dt.strftime(\"%Y-%m-%d\").data}', \n",
    "                     ha='right', fontname='Courier New',\n",
    "                     transform=ax.transAxes, \n",
    "                     bbox=dict(boxstyle='round', facecolor='whitesmoke'))\n",
    "\n",
    "levels = 20          # number of contour levels\n",
    "cmap = 'RdYlBu_r'    # choose colormap\n",
    "\n",
    "p = [ ax.contourf(lon, lat, var[0,:,:], levels=levels, cmap=cmap ) ]\n",
    "\n",
    "def update(i):\n",
    "    for tp in p[0].collections:\n",
    "        tp.remove()\n",
    "    date_label.set_text(f'{ds.time[i].dt.strftime(\"%Y-%m-%d\").data}')\n",
    "    p[0] = ax.contourf(lon, lat, var[i,:,:], levels=levels, cmap=cmap ) \n",
    "    return p[0].collections+[date_label]\n",
    "\n",
    "ani = anim.FuncAnimation(fig, update, frames=len(ds.time), interval=100, \n",
    "                              blit=True, repeat=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8f40444-4f52-47d9-8194-fc2f70a95739",
   "metadata": {},
   "source": [
    "Call the animation object _ani_ to display the animation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7877e48c-6143-4ca1-bb67-380810ddda7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "ani"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67a0f696-c567-4c2b-8ed5-e6458e71d846",
   "metadata": {},
   "source": [
    "## Save the animation to MPEG-4\n",
    "\n",
    "The animation can now be save to a MPEG-4 file using the `save` method of the animation object. Matplotlib provides beside others the pipe-based `FFMpegWriter` which we use in the second example below. \n",
    "\n",
    "The easiest way is simply to use the default settings for the FFMpegWriter, which is done by setting the `writer='ffmpeg'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3caeffbd-4844-4444-a692-d411d311a84d",
   "metadata": {},
   "outputs": [],
   "source": [
    "ani.save('air_temperature_1.mp4', writer='ffmpeg')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f83eba9e-d793-42f3-a112-94e7188b0e66",
   "metadata": {},
   "source": [
    "The pipe-based `FFMpegWriter` streams and writes the frames in a single pass. The framerate, codec, and bitrate can be set manually. Additional metadata like title, artist, genre, subject, copyright, etc. can be set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2279f1af-45a0-47bf-8846-255c9543164e",
   "metadata": {},
   "outputs": [],
   "source": [
    "metadata = {'title':'Air Temperature'}\n",
    "\n",
    "FFMwriter = anim.FFMpegWriter(fps=25, extra_args=['-vcodec', 'libx264'], metadata=metadata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c9792c8-437a-40aa-9b38-529a151d7a6c",
   "metadata": {},
   "outputs": [],
   "source": [
    "ani.save('air_temperature_2.mp4', writer=FFMwriter)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4f1d023-753f-4e64-8f97-6b25b95b4709",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "cartopy",
   "language": "python",
   "name": "cartopy"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
