# convesion tasks for pdf files
%.pdf: %.md
	pandoc -o $@  $^
%.tex: %.ipynb
	jupyter nbconvert $^ --to latex 
%.pdf: %.tex
	xelatex $^
