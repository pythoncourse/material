# DKRZ Python course materials

[![Gitlab-repo](https://img.shields.io/badge/gitlab-repo-green)](https://gitlab.dkrz.de/pythoncourse/material)
[![Pages](https://img.shields.io/badge/gitlab-pages-blue)](https://pythoncourse.gitlab-pages.dkrz.de/material)
[![Jupyterlite](https://img.shields.io/badge/exec-jupyterlite-red)](https://pythoncourse.gitlab-pages.dkrz.de/material/jupyterlite)

## Directories

| Section | Description |
|---|---|
| [notebooks](https://gitlab.dkrz.de/pythoncourse/material/-/tree/master/notebooks) | Basis content of the workshop |
| [data](https://gitlab.dkrz.de/pythoncourse/material/-/tree/master/data)      | data files for examples and exercises |
| [google-doc](https://docs.google.com/document/d/1RU9o2w1Hwm7tZStfXwihom8VgLbAfUgaAMzucy74tGE/edit) | living doc for your questions and basic commands |

# Agenda

## Day 1 , 2024-03-05

| Starttime  | Endtime  | Title | Content |
|:-----------|:---------|:------|:--------|
| 09:00 | 09:10 | Welcome | A short welcome to the participants and speakers. |
| 09:10 | 09:30 | Difference between a supercomputer and a mobile phone | - what is a node, cpu, historical development, architecture |
| 09:30 | 10:00 | What is Python – short intro | - difference between programming language and Python, assembler, compiler, interpreter |
| 10:00 | 11:00 | Jupyter Notebooks | - connecting to Levante with Jupyterhub, Jupyterlab, Installation |
| 11:00 | 11:15 | <b>Break</b> | | |
| 11:15 | 12:30 | Introduction to Linux | - terminal, via WSL, command line and shell, editor |
| 12:30 | 13:30 | <b>Break</b> | | |
| 13:30 | 14:00 | Syntax | - case-sensitive, print, indentation and code blocks, line continuation, comments |
| 14:00 | 14:15 | Data types | - boolean, integer, floating-point, complex, conversion between types, None |
| 14:15 | 14:30 | Strings | - formatting, string functions |
| 14:30 | 14:45 | Lists, tuples, and sets |  |
| 14:45 | 15:15 | Dictionaries | - creation, access |

## Day 2 , 2024-03-06

| Starttime  | Endtime  | Title | Content |
|:-----------|:---------|:------|:--------|
| 09:00 | 09:15 | Warmup about content of Day 1 |  |
| 09:15 | 09:30 | Copy objects | - shallow, deep |
| 09:30 | 09:45 | Basic operators and math functions |  |
| 09:45 | 10:15 | Conditions |  |
| 10:15 | 10:45 | Loops | - for, while |
| 10:45 | 11:00 | <b>Break</b> | | |
| 11:00 | 11:30 | List comprehensions | - nested |
| 11:30 | 11:45 | Functions |  |
| 11:45 | 12:45 | File I/O | - open, close, read, write, csv reader |
| 12:45 | 13:45 | <b>Break</b> | | |
| 13:45 | 14:30 | Exceptions, error handling | - try and except, logging, debugging |
| 14:30 | 15:30 | Python in Action (GLOBAGRIM) |  |

## Day 3 , 2024-03-07

| Starttime  | Endtime  | Title | Content |
|:-----------|:---------|:------|:--------|
| 09:00 | 09:15 | Warmup about content of Day 2 |  |
| 09:15 | 10:45 | Numpy |  |
| 10:45 | 11:00 | <b>Break</b> | | |
| 11:00 | 12:30 | Xarray I | - DataArray, Dataset, open_dataset / open_mfdataset |
| 12:30 | 13:30 | <b>Break</b> | | |
| 13:30 | 15:30 | Visualization I | - Matplotlib |

## Day 4 , 2024-03-08

| Starttime  | Endtime  | Title | Content |
|:-----------|:---------|:------|:--------|
| 09:00 | 09:15 | Warmup about content of Day 3 |  |
| 09:15 | 10:15 | Visualization II | - Cartopy |
| 10:15 | 11:15 | Xarray II |  |
| 11:15 | 11:45 | <b>Break</b> | | |
| 11:45 | 12:45 | Xarray III |  |
| 12:45 | 13:00 | Feedback |  |
| 13:00 | 15:00 | Questions and answers |  |
