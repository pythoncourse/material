# DKRZ Python course materials

[![Gitlab-repo](https://img.shields.io/badge/gitlab-repo-green)](https://gitlab.dkrz.de/pythoncourse/material)
[![Pages](https://img.shields.io/badge/gitlab-pages-blue)](https://pythoncourse.gitlab-pages.dkrz.de/material)
[![Jupyterlite](https://img.shields.io/badge/exec-jupyterlite-red)](https://pythoncourse.gitlab-pages.dkrz.de/material/jupyterlite)

## Directories

| Section | Description |
|---|---|
| [notebooks](https://gitlab.dkrz.de/pythoncourse/material/-/tree/master/notebooks) | Basis content of the workshop |
| [data](https://gitlab.dkrz.de/pythoncourse/material/-/tree/master/data)      | data files for examples and exercises |

# Agenda
