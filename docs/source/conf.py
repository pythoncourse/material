# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'DKRZ Python Course'
copyright = '2020-2025, DKRZ'
author = 'Python course team: Karin, Ralf, Marco, Jannek, Angelika, plus Nils-Arne'

# The full version, including alpha/beta/rc tags
release = '8.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
  "nbsphinx",
  "myst_parser",
  "sphinx_copybutton"
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

nbsphinx_allow_errors = True
nbsphinx_kernel_name = 'anaconda3_bleeding'
#nbsphinx_execute = 'never'
jupyter_execute_notebooks = "on"

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'
html_theme_options = {
    #'show_prev_next': False,
    'repository_url': 'https://gitlab.dkrz.de/pythoncourse/material', #'https://github.com/spatialaudio/nbsphinx',
    'use_repository_button': True,
    'use_edit_page_button': False,
    'use_fullscreen_button' : True,
    'use_download_button' : True
}
html_context = {
#    'github_user': 'spatialaudio',
#    'github_repo': 'nbsphinx',
#    'github_version': 'master',
#    'doc_path': 'doc',
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
