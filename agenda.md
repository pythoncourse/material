## Day 1 , 2025-03-31  Short Introduction to Jupyterhub and Linux for those who are interested

| Starttime   | Endtime   | Title                                                         | Content                                                                                                         |
|:------------|:----------|:--------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------|
| 13:30       | 13:40     | <b>Welcome</b>                                                | <ul><li>A short welcome to the participants and speakers.</li></ul> |
| 13:40       | 14:00     | Difference between a supercomputer and your mobile phone      | <ul><li> What is a node, cpu</li><li>  Historical development</li><li> Architecture</li></ul>  |
| 14:00       | 14:15     | What is Python – short intro                                  | <ul><li> Difference between programming language and Python</li><li> Assembler, compiler, interpreter</li></ul> |
| 14:15       | 15:00     | Jupyter notebooks                                             | <ul><li>connecting to Levante with Jupyter hub</li><li> Jupyter lab</li><li>Installation</li></ul>  |
| 15:00       | 15:15     | <b>Break</b>  |  |
| 15:15       | 16:15     | Introduction to Linux system and setting up work environment  | <ul><li> Terminal</li><li> WSL</li><li> Command line and shell</li><li> Editor</li></ul>  |


## Day 2 , 2025-04-01  Start of the Python course

| Starttime   | Endtime   | Title                                                         | Content                                                                                                         |
|:------------|:----------|:--------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------|
| 09:00       | 09:30     | <b>Welcome</b>                                                | <ul><li>Jupyterhub</li><li>Jupyterlab</li><li>terminal</li><li>copy course material</li></ul> |
| 09:30       | 10:00     | Syntax     | <ul><li>case-sensitive</li><li>print</li><li>indentation and code blocks</li><li>line continuation</li><li>comments</li></ul> |
| 10:00       | 10:15     | Data types | <ul><li>boolean</li><li>integer</li><li>floating-point</li><li>complex</li><li>conversion between types</li><li>None</li></ul> |
| 10:15       | 10:30     | Strings    | <ul><li>formatting</li><li>string functions</li></ul> |
| 10:30       | 10:45     | Lists, tuples, and sets | |
| 10:45       | 11:15     | Dictionaries  | <ul><li>creation</li><li>access</li></ul> |
| 11:15       | 11:30     | <b>Coffee break</b> |  |
| 11:30       | 11:45     | Copy objects  | <ul><li>shallow copy</li><li>deep copy</li></ul> |
| 11:45       | 12:00     | Basic operations |  |
| 12:00       | 12:30     | Conditions    |  |
| 12:30       | 13:30     | <b>Lunch</b> |  |
| 13:30       | 14:00     | Loops         | <ul><li>for</li><li>while</li></ul> |
| 14:00       | 14:30     | List comprehension | <ul><li>definition</li><li>nested</li></ul> |
| 14:30       | 14:45     | Functions     | <ul><li>definition</li></ul> |
| 14:45       | 15:00     | <b>Coffee break</b> |  |
| 15:00       | 15:45     | File I/O      | <ul><li>open</li><li>close</li><li>read</li><li>write</li><li>csv reader</li></ul> |
| 15:45       | 16:15     | Exceptions, error handling | <ul><li>try and except</li><li>logging</li><li>debugging</li></ul> |


## Day 3 , 2025-04-02

| Starttime   | Endtime   | Title                            | Content                                                                                                                                                                |
|:------------|:----------|:---------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 09:00       | 09:05     | <b>Warmup about content of Day 1</b>    |  |
| 09:05       | 10:30     | Numpy (1/1)     |  |
| 10:30       | 10:45     | <b>Coffee break</b> |  |
| 10:45       | 11:30     | Numpy (1/2)     |  |
| 11:30       | 12:30     | Xarray (1/1)     |  |
| 12:30       | 13:30     | <b>Lunch</b>  |  |
| 13:30       | 15:00     | Xarray (1/2)     |  |
| 15:00       | 15:15     | <b>Coffee break</b> |  |
| 15:15       | 16:15     | Xarray (1/3)     |  |


## Day 4 , 2025-04-03

| Starttime   | Endtime   | Title                                     | Content                                                 |
|:------------|:----------|:------------------------------------------|:--------------------------------------------------------|
| 09:00       | 09:05     | <b>Warmup about content of Day 2</b>   |   |
| 09:05       | 11:00     | Visualization (1/1)          |   |
| 11:00       | 11:15     | <b>Coffee break</b> |  |
| 11:15       | 12:30     | Visualization (1/2)          |   |
| 12:30       | 13:30     | <b>Lunch</b>  |  |
| 13:30       | 15:00     | Visualization (2/1)          |   |
| 15:00       | 15:15     | <b>Coffee break</b> |  |
| 15:15       | 16:30     | Visualization (2/2)          |   |


## Day 5 , 2025-04-04

| Starttime   | Endtime   | Title                         | Content                                                                 |
|:------------|:----------|:------------------------------|:------------------------------------------------------------------------|
| 09:00       | 09:05     | <b>Warmup about content of Day 3</b> |  |
| 09:05       | 10:45     | Xarray (2/1)     |  |
| 10:45       | 11:00     | <b>Coffee break</b> |  |
| 11:00       | 12:30     | Xarray (2/2)     |  |
| 12:30       | 13:30     | <b>Lunch</b>  |  |
| 13:30       | 14:30     | Xarray (2/3)     |  |
| 14:30       | 15:15     | Python in action - GLOBAGRIM     | <ul><li>an application example</li></ul> |
| 15:15       | 16:00     | <b>Time for feedback</b>  |   |
