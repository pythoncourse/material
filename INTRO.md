# Difference between levante and a mobile phone

Because this is not the central part of the course and contains no executable
code, it will be more a collection of useful information rather than a detailed
description of the topic.

Hardware and Software developed side-by-side starting the first half of the 20th
century mainly as industrial tools to push forward automation in production and
data assesment and analysis but also from the military area.
## Hardware
Notable mentions:
- Z1: first programmable computer by Konrad Zuse around 1937, purely mechanical, privately financed
![Z1 image](https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/German_Museum_of_Technology%2C_Berlin_2017_024.jpg/1920px-German_Museum_of_Technology%2C_Berlin_2017_024.jpg "Z1 in the museum today")
- ENIAC: first programmable electronic computer, 1945-1956, 18000 tubes, Army research Lab
![ENIAC image](https://upload.wikimedia.org/wikipedia/commons/d/d3/Glen_Beck_and_Betty_Snyder_program_the_ENIAC_in_building_328_at_the_Ballistic_Research_Laboratory.jpg "ENIAC")

Mechanical was not reliable, so the rapid miniaturiaztion for electronic parts pushed the computer
 hardware from tubes to transistors to [microprocessors beginning in the 1960s](https://learn.saylor.org/mod/page/view.php?id=22020)

If you are more interested in the history part, check [IBM](https://en.wikipedia.org/wiki/IBM). It shows the close relation between computing, industry, military and society over more than a century.

### Basic parts of a computer system

Almost all computers follow the so-called [von Neumann architecture](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Von_Neumann_Architecture.svg/720px-Von_Neumann_Architecture.svg.png) from 1945:
- Input and Output 
- Central processing for arithmetics/logics and memory controlling
- Memory

What a computer can do is basically
- write to memory
- read from memory
- compute with the memory

Different chipsets offer different operations with single instructions collected under the term [Assembler or Assembly language](https://en.wikipedia.org/wiki/Assembly_language#Assembler). Hardware architecture you might know: 
- X86 32/64bit: XBox, Playstation 4/5, Laptops, Desktops, Levante
- Arm: smartfon, tablet, recent HPC-systems like Fugaku
- MIPS: 64bit, routers
- RISC: old SGI HPC systems
- PowerPC: based on RISC, IBM PC and HPC systems, Apple hardware before Intel-aera, Playstation 3

### Memory in action

input program
```c
#include <stdlib.h>                                                            
#include <stdio.h>
#include <math.h>
 
int main(int argc, char **argv) {
  double x = atof(argv[1]);
  double result = x+x;
  printf("results %f\n", result);
  return 0;
}
```

Compile it with `gcc -S addition.c` to assembly on two systems with different GCC compiler leads to two slightly different files. See `addition-laptop.s` and `addition-mistral.s` in the repository.

![Left: laptop gcc-11.2.0 | Right Mistral gcc-4.4.7](https://gitlab.dkrz.de/pythoncourse/material/-/raw/master/addition-diff.png?inline=false "Left: laptop gcc-11.2.0 | Right Mistral gcc-4.4.7")

### Memory Hierarchy
| type | access | size | cost |
|--|--|--|--|
| registers |5ns |1e2| part of CPU |
|caches (SRAM)| 10ns |1e6 | 100.00 $/MB |
|main memory (DRAM)| 100ns | 1e9 | 1.00 $/MB|
|hard disk |5000ns |1e11  | .05 $/MB|

![Cache in real life](https://miro.medium.com/max/538/1*4SZ3hUZHJKCd-mxL2YolQg.png "memory in real life")
Real Size: 1cmx1cm

## Software

Together with programmable hardware the languages evolved. Hence Konrad Zuse was again the first:
[_Plankalkühl_](http://zuse.zib.de/item/DB2j_t_w1fbxvaiq) non-published book from 1945 because of WWII. Another old standing member in the family of programming languages is FORTRAN from 1950s ([Good overview of languages](https://learn.saylor.org/mod/page/view.php?id=22022))

Basic goal:
- expression of mathematical formual (computation!)
- expression of algorithms -> solving problems!


## How to put software into action

As mentioned above software exists in different abtraction levels but today mostly in the form a text file. In the early days [punch cards](https://en.wikipedia.org/wiki/Punched_card) were used instead because there where easier to read by machines ([Fortran example](https://upload.wikimedia.org/wikipedia/commons/5/58/FortranCardPROJ039.agr.jpg)). But let's stick to text files:

### Basic compilation process
![compilation process](https://miro.medium.com/max/1036/1*wHKe6W4opLmk6pb7sxZz6w.png)

The compiler translates the source through several step into a machine language and outputs an executable program, which can then later be used as often as needed. 

But Python is *NOT* a compiled language! True, the difference is not so huge. here is how it works with python:

![python interpreter process](https://i2.wp.com/techvidvan.com/tutorials/wp-content/uploads/sites/2/2020/03/how-python-interpreter-works.jpg?ssl=1)

In interpreter incorporates all necessary steps of the compiler and linker *AND* executes the program. Both methods, Compilation and Interpretation have pros and cons:
- compiled programms need to be re-compiled every time the source code is changed
- compiled programms contain machine code, only. This gives a big advantage in runtime performance
- interpreter don't bother the programer with complex compilation processes and linking problems
- programs for an interpreted language are _source code_: very easy to debug and change
- the development cycle with an interpreter language is faster, because there is no extra compilation step. Of course there are techniques to combine both types of languages ;-)

Example from above in python (invented by [Guido von Rossum](https://gvanrossum.github.io/))
```python
import sys
x = float(sys.argv[1])
result = x+x
print("result = %s",result)
```
Running (Compilation + Execution) with `python addition.py 3`

## Further readings

[This collection of free online books](https://www.sciencebooksonline.info/computer-science.html) is a good start

## Levante configuration

https://docs.dkrz.de/doc/levante/configuration.html
